import {ITransaction} from "./ITransaction";

export class Transaction implements ITransaction {
  amount: number;
  effectiveDate: Date;
  id: string;
  type: string;

}

