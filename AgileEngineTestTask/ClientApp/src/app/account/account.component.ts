import {Component, Inject} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {ITransaction} from "./ITransaction";
import {Transaction} from "./Transaction";

@Component({
  selector: 'app-account-component',
  templateUrl: './account.component.html'
})
export class AccountComponent {

  public currentMoney: number;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
    this.getBalance();
  }

  public Credit(amount: number) {
    this.postTransaction('Credit', amount)
  }

  public Debet(amount: number) {
    this.postTransaction('Debet', amount)
  }

  postTransaction(_type:string, _amount: number){
    this.http.post(this.baseUrl + 'api/Transaction/',
      {
        type: _type,
        amount: _amount
      })
      .subscribe(
        (val) => {
          console.log("POST call successful value returned in body",
            val);
        },
        response => {
          console.log("POST call in error", response);
        },
        () => {
          console.log("The POST observable is now completed.");
          this.getBalance();
        });
  }

  getBalance() {
    return this.http.get(this.baseUrl + 'api/Transaction/getBalance')
      .subscribe((amount: number) => {this.currentMoney = amount});
  }

  getTransaction(id: number): Observable<ITransaction>{
    return this.http.get<ITransaction>(this.baseUrl + 'api/Transaction/getTransaction', {
      params: new HttpParams({
        fromObject: {
          id: id.toString(),
        }})
    });
  }
}
