import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ITransaction } from "../account/ITransaction";

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html',
  styleUrls: ['./fetch-data.component.css']
})
export class FetchDataComponent {
  public transactions: ITransaction[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<ITransaction[]>(baseUrl + 'api/Transaction/allTransactions').subscribe(result => {
      this.transactions = result;
    }, error => console.error(error));
  }

  selectedTransaction: ITransaction;
  onSelect(transaction: ITransaction): void {
    this.selectedTransaction = transaction;
  }
}
