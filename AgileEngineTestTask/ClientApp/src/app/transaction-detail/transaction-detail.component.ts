import { Component, OnInit, Input } from '@angular/core';
import { ITransaction } from "../account/ITransaction";

@Component({
  selector: 'app-transaction-detail',
  templateUrl: './transaction-detail.component.html',
  // styleUrls: ['./transaction-detail.component.css']
})
export class TransactionDetailComponent implements OnInit {
  @Input() transaction: ITransaction;

  constructor() { }

  ngOnInit() {
  }

}

