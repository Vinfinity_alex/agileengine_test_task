using AgileEngineTestTask.Models;
using Microsoft.EntityFrameworkCore;

namespace AgileEngineTestTask.ServerApp
{
    public class AppContext : DbContext
    {
        public AppContext(DbContextOptions<AppContext> options) : base(options) { }

        public DbSet<Transaction> Transactions { get; set; }
    }
}