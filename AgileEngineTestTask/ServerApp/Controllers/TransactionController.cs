using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AgileEngineTestTask.Models;
using AgileEngineTestTask.ServerApp;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AppContext = AgileEngineTestTask.ServerApp.AppContext;

namespace AgileEngineTestTask.Controllers
{
    [Route("api/[controller]")]
    public class TransactionController : Controller
    {
        private AppContext _appContext;

        public TransactionController(AppContext appContext)
        {
            _appContext = appContext;
            _appContext.SaveChanges();
        }


        [HttpGet("[action]")]
        public async Task<IEnumerable<Transaction>> allTransactions()
        {
            var trans = _appContext.Transactions.ToArray();
            for (int id = 0; id < trans.Length; id++)
            {
                trans[id].Id = id.ToString();
            }
            return await _appContext.Transactions.ToListAsync();
        }

        [HttpGet("[action]")]
        public async Task<Transaction> getTransaction(string id)
        {
            return await _appContext.Transactions.FirstOrDefaultAsync(x => x.Id == id);
        }

        [HttpGet("[action]")]
        public async Task<int> getBalance()
        {
            return await Task.Run(() => _appContext.Transactions.Last().Amount);
        }


        [HttpPost]
        public async Task saveTransaction([FromBody]Transaction transact)
        {
            using (var context = _appContext)
            {
                //using (var transaction = context.Database.BeginTransaction())
                //{
                    try
                    {
                        var trans = new Transaction
                        {
                            Type = transact.Type,
                            Amount = transact.Amount,
                            EffectiveDate = DateTime.Now.ToString()
                        };

                        var balance = 0;
                        if (context.Transactions.Any())
                            balance = context.Transactions.Last().Amount;
                        if (trans.Type == TransactionType.Credit.ToString())
                        {
                            if(balance - trans.Amount < 0) 
                                throw new Exception("Zero balance");
                            
                            trans.Amount = balance - trans.Amount;
                        }
                        else
                            trans.Amount += balance;

                        context.Transactions.Add(trans);
                        context.SaveChanges();

                        //transaction.Commit();
                    }
                    catch (Exception)
                    {
                        // TODO: Handle failure
                    }
                //}
            }
        }
    }
}