using System;

namespace AgileEngineTestTask.Models
{
    public class Transaction
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public int Amount { get; set; }
        public string EffectiveDate { get; set; }
        public int CurrentAmount { get; set; }
    }
}